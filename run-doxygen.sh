#!/bin/bash

# This script takes one argument which is the name of the project which contains the doxyfile.

module_name=$1
echo "Build documentation for module $module_name:"
cd $module_name
doxygen
cd ..
