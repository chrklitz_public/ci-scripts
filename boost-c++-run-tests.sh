#!/bin/bash

# This script takes one argument which is the name of the project which should be tested.
# This then compiles and executes the tests.

module_name=$1
echo "Compile and run tests on module $module_name:"
mkdir build-test
cd build-test
qmake -qt=qt5 ../$module_name/$module_name.pro "CONFIG+=test"
make
chmod +x $module_name
./$module_name
cd ..
